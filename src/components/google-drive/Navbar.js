import React from "react";
import { Navbar, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function NavbarComponent() {
  return (
    <Navbar bg="light" expand="sm">
      <Navbar.Brand as={Link} to="/">
        <div>
          <img
            style={{ height: "40px" }}
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/Google_Drive_icon_%282020%29.svg/2295px-Google_Drive_icon_%282020%29.svg.png"
            alt="Drive"
          />
        </div>
      </Navbar.Brand>
      <Nav>
        <Nav.Link as={Link} to="/user">
          <h3>Drive Profile</h3>
        </Nav.Link>
      </Nav>
    </Navbar>
  );
}
